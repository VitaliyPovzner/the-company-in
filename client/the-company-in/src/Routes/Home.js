import "../App.css";
import Axios from "axios";
import { useState, useEffect } from "react";
import Company from "../Components/Company";

function Home() {
    const [companies, setCompanies] = useState([]);
    const [searchTerm, setSearchTerm] = useState("");

    const handleChange = (e) => {
        setSearchTerm(e.target.value);
        console.log()
    };

    const handleSearch = (e) =>{
      console.log(searchTerm)
    }

    return (
        <div className="App">
            <div className="headerContainer">
                <h1>Welcome to the Company in</h1>
                <div className="buttonContainer">
                    <input
                        placeholder="Search for company information"
                        type="text"
                        onChange={handleChange}
                    />
                    <button onClick={handleSearch} >Search</button>
                </div>
            </div>
        </div>
    );
}
export default Home;

