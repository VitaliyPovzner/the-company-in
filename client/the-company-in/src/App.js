import React from "react";
import "./App.css";
import {
  BrowserRouter,
  Routes,
  Route
} from "react-router-dom";
import Home from "./Routes/Home.js";
import CompanyPage from "./Routes/Company.js";

function App() {
  return (
    <div className="App">
      {/* <BrowserRouter>
        <Routes>
          <Route path="/" exact render={(props) => <Home />} />
          <Route path="/CompanyPage/:domain" exact render={(props) => <CompanyPage />} />
        </Routes>
      </BrowserRouter> */}
      <Home />
    </div>
  );
}

export default App;