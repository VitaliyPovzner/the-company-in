const axios = require('axios');
const INFO_HOST = process.env.INFO_HOST;
axios.defaults.headers.common = { 'Authorization': 'Bearer sk_30240e2d1dfc1d73d26ab80390d1fd49' };


module.exports = {
    getCompanyDetails: async function (domain) {
        console.log(`getCompanyDetails called with domain: ${domain}`);
        try {
            const result = await axios.get(`${INFO_HOST}/find?domain=${domain}`);
            return result
        } catch (error) {
            console.error(error, 'Error during get company details');
        }
    }
}