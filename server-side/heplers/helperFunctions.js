const SUPPORTED_QUERY = ['domain'];
const BAD_QUERY_MSG = `Only one query param is supported, and it must be one of the following: ${SUPPORTED_QUERY.join(', ')}`;


function checkQueryParams(query){
    const valid =true;
    let queryKeys = Object.keys(query);
    if (queryKeys.length > 1 || queryKeys.length === 0 || !SUPPORTED_QUERY.includes(queryKeys[0].toLowerCase())) {
        return !valid;
    }
    return valid;
}

exports.checkQueryParams = checkQueryParams;