const express = require('express');
const app = express();
const port = process.env.PORT || 8080;
const cors = require('cors');
app.use(cors());



const CompanyController = require('./controllers/CompanyController');
app.use('/companies', CompanyController);

app.use((req, res) => {
  res.status(404).json({ message: 'Path not found, only the following paths are supported: GET /comanies/?domain=domain '});
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})