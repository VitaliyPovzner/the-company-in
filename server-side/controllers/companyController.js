require('dotenv').config()
const express = require('express');
const router = express.Router();
const helpers = require('../heplers/helperFunctions');
const companyService = require('../service/companyService');
const BAD_QUERY_MSG = `Only one query param is supported, and it must be one of the following: domain`;



///routes
router.get('/', getCompanyDetails);


//functions
async function getCompanyDetails(req, res) {
    const domain = req.query.domain;
    const query = req.query;
    // Make sure only valid query params were passed:
    if (!helpers.checkQueryParams(query)) {
        res.status(500).json({ message: BAD_QUERY_MSG });
        return;
    }
    try {
        const result = await companyService.getCompanyDetails(domain);
        res.send(result.data);
    } catch (error) {
        res.sendStatus(404);
    }
}

module.exports = router;